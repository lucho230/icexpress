/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distrital.icexpress.platform.model;

/**
 *
 * @author LUIS
 */

public class Usuario {
    
    private String userName;
    private String userIdentificationNumber;
    private String userNivelProfesional;
    private String userCelular;
    private String userCreditCard;
    private String userEmail;
    private String userCity;
    private String userPassword;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserIdentificationNumber() {
        return userIdentificationNumber;
    }

    public void setUserIdentificationNumber(String userIdentificationNumber) {
        this.userIdentificationNumber = userIdentificationNumber;
    }

    public String getUserCelular() {
        return userCelular;
    }

    public void setUserCelular(String userCelular) {
        this.userCelular = userCelular;
    }

    public String getUserCreditCard() {
        return userCreditCard;
    }

    public void setUserCreditCard(String userCreditCard) {
        this.userCreditCard = userCreditCard;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserNivelProfesional() {
        return userNivelProfesional;
    }

    public void setUserNivelProfesional(String userNivelProfesional) {
        this.userNivelProfesional = userNivelProfesional;
    }
}
