/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distrital.icexpress.platform.util;

/**
 *
 * @author LUIS
 */
public class ForoConstants {
    
    public static final String FORM_FIELD_USER = "user_name";
    public static final String FORM_FIELD_PASS = "user_pass";
    
    public static final String MSM_CAMPOS_OBLIGATORIOS = "Campos obligatorios!";
    public static final String URL_CONSULTAR_USUARIO_POR_ID = "https://icexpress-usuarios-dot-api-project-912571974900.appspot.com/_ah/api/icexpress/v1/getting";
    public static final String URL_INGRESAR_USUARIO = "https://icexpress-usuarios-dot-api-project-912571974900.appspot.com/_ah/api/icexpress/v1/receive";
    public static final String URL_AGREGAR_FORO = "https://icexpress-foros-dot-api-project-912571974900.appspot.com/_ah/api/icexpress/v1/addforo";
    
    public static final String PARAMS_AGREGAR_FORO = "{\"userCedula\": \"param1\", \"foroNombre\": \"param2\", \"foroContenido\": \"param3\"}";
    public static final String PARAMS_INSERTAR_USUARIO = "{\"userName\": \"param1\", \"userIdentificationNumber\": \"param2\", \"userNivelProfesional\": \"param3\", \"userCelular\": \"param4\", \"userEmail\": \"param5\", \"userCity\": \"param6\", \"userPassword\": \"param7\"}";
    
    
    
}
