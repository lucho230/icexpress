/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distrital.icexpress.platform.controllers;

import com.distrital.icexpress.platform.delegate.ForoDelegate;
import com.distrital.icexpress.platform.model.Usuario;
import com.distrital.icexpress.platform.util.ForoConstants;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author LUIS
 */
@Controller
@RequestMapping(value = "/user")
public class ForoController {
    
    private static final Logger _logger = Logger.getLogger(ForoController.class.getName());
    
    @RequestMapping(value = "/login", method = {RequestMethod.POST})
    public ModelAndView iniciaSesion(HttpServletRequest request) {
        
        ForoDelegate delegateForo = new ForoDelegate();
        ModelAndView view = new ModelAndView("user/login/sesion");
        String user_cedula = request.getParameter(ForoConstants.FORM_FIELD_USER);
        String user_pass = request.getParameter(ForoConstants.FORM_FIELD_PASS);
        
        if (user_cedula != null && !user_cedula.isEmpty()  &&
            user_pass != null   && !user_pass.isEmpty()) {
            
            Usuario dataUser = delegateForo.consultaUsuarioPorCedula(user_cedula, user_pass);
            
            if (dataUser != null ){
                System.out.println("BIENVENIDO");
                view.addObject("usuario", dataUser);
            }  else {
                System.out.println("FAIL!");
                view = new ModelAndView("index");
            }
        } else {
            view.addObject("error", ForoConstants.MSM_CAMPOS_OBLIGATORIOS);
        }
        return view;
    }
    
    @RequestMapping(value = "/foro", method = {RequestMethod.POST})
    public ModelAndView guardarForo(HttpServletRequest request) {
        
        ModelAndView view = new ModelAndView("user/login/sesion");
        ForoDelegate delegateForo = new ForoDelegate();
        
        String foroNombre = request.getParameter("foro_nombre");
        String foroContenido = request.getParameter("foro_contenido");
        
        if (foroNombre    != null && !foroNombre.isEmpty()  &&
            foroContenido != null && !foroContenido.isEmpty()) {
            if (delegateForo.publicarContenido(foroNombre, foroContenido)) {
                view.addObject("response", "success");
            } else {
                view.addObject("response", "error");
            }
        }

        return view;
    }
    
    @RequestMapping(value = "/insertar", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView redirectInsertar(HttpServletRequest request) {
        return new ModelAndView("user/login/registrarse");
    }
    
    @RequestMapping(value = "/registro", method = {RequestMethod.POST})
    public ModelAndView insertarUsuario(HttpServletRequest request) {
        
        ModelAndView view = new ModelAndView("user/login/registrarse");
        Usuario user = new Usuario();
        ForoDelegate delegate = new ForoDelegate();
        
        String userNombre = request.getParameter("user_name");
        String userPassword = request.getParameter("user_pass");
        String userCedula = request.getParameter("user_cedula");
        String userEmail = request.getParameter("user_email");
        String userNivelProfesional = request.getParameter("user_profesion");
        String userCelular = request.getParameter("user_telefono");
        String userCiudad = request.getParameter("user_ciudad");
        
        if (userNombre != null   && !userNombre.isEmpty()   &&
            userPassword != null && !userPassword.isEmpty() && 
            userCedula != null   && !userCedula.isEmpty()   &&
            userEmail != null    && !userEmail.isEmpty() ) {
            user.setUserName(userNombre);
            user.setUserPassword(userPassword);
            user.setUserIdentificationNumber(userCedula);
            user.setUserCelular(userCelular);
            user.setUserEmail(userEmail);
            user.setUserCity(userCiudad);
            user.setUserNivelProfesional(userNivelProfesional);
            
            if (delegate.insertarUsuario(user)) {
                view.addObject("responseUser", "success");
            } else {
                view.addObject("responseUser", "error");
            }
        } else {
            _logger.log(Level.INFO, "Campos incompletos!");
        }
        
        return view;
    }
}
