/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distrital.icexpress.platform.delegate;

import com.distrital.icexpress.platform.controllers.ForoController;
import com.distrital.icexpress.platform.model.Usuario;
import com.distrital.icexpress.platform.util.ForoConstants;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author LUIS
 */
public class ForoDelegate {
    
    private static final Logger _logger = Logger.getLogger(ForoDelegate.class.getName());
    private JsonObject objJson =  new JsonObject();
    
    public Usuario consultaUsuarioPorCedula (String cedula, String pass) {
        
        Usuario user = new Usuario();
        try {
            Client client = Client.create();
            
            WebResource webResource = client        
                .resource(ForoConstants.URL_CONSULTAR_USUARIO_POR_ID);

            MultivaluedMap formData = new MultivaluedMapImpl();
            formData.add("dni", cedula);
            ClientResponse response = webResource.type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
  
            String output = response.getEntity(String.class);
            System.out.println("Output from Server .... \n");
            System.out.println(output);
            
            if (output != null && !output.isEmpty()) {
                JsonObject responseUsuario = (JsonObject) new JsonParser().parse(output);
                String status = responseUsuario.get("code").getAsString();
                
                if (status != null && !status.isEmpty() && status.equals("112")) {
                    JsonObject datosUsuario = (JsonObject) responseUsuario.get("usuario");
                    user.setUserName(datosUsuario.get("userName").getAsString());
                    user.setUserPassword(datosUsuario.get("userPassword").getAsString());
                    user.setUserIdentificationNumber(datosUsuario.get("userIdentificationNumber").getAsString());
                    user.setUserEmail(datosUsuario.get("userEmail").getAsString());
                    user.setUserCreditCard(datosUsuario.get("userCreditCard").getAsString());
                    user.setUserCity(datosUsuario.get("userCity").getAsString());
                    user.setUserCelular(datosUsuario.get("userCelular").getAsString());
                    if (!datosUsuario.get("userPassword").getAsString().equals(pass)) {
                        user = null;
                    }
                }
            }
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            _logger.log(Level.WARNING, "ERROR - ForoDelegate.consultaUsuarioPorCedula, al consultar servicio REST usuarios por dni {0}", e.getMessage());
            return null;
        }
    }
    
    public boolean publicarContenido (String body, String content) {
        
        Usuario user = new Usuario();
        boolean respuesta = false;
        try {
            Client client = Client.create();
            
            WebResource webResource = client        
                .resource(ForoConstants.URL_AGREGAR_FORO);
            
            ClientResponse response = webResource.type("application/json")
                   .post(ClientResponse.class, ForoConstants.PARAMS_AGREGAR_FORO.replace("param1", "1010195130").replace("param2", body).replace("param3", content));
  
            String output = response.getEntity(String.class);
            System.out.println("Output from Server .... \n");
            System.out.println(output);
                
            if (output != null && !output.isEmpty()) {
                JsonObject responseUsuario = (JsonObject) new JsonParser().parse(output);
                String status = responseUsuario.get("code").getAsString();
                
                if (status != null && !status.isEmpty() && status.equals("106")) {
                    respuesta =  true;
                }
            }
            return respuesta;
        } catch (Exception e) {
            e.printStackTrace();
            _logger.log(Level.WARNING, "ERROR - ForoDelegate.consultaUsuarioPorCedula, al consultar servicio REST usuarios por dni {0}", e.getMessage());
            return respuesta;
        }
    }
    
    
     public boolean insertarUsuario (Usuario usuario) {
        
        boolean respuesta = false;
        try {
            Client client = Client.create();
            
            WebResource webResource = client        
                .resource(ForoConstants.URL_INGRESAR_USUARIO);
            
            ClientResponse response = webResource.type("application/json")
                   .post(ClientResponse.class, ForoConstants.PARAMS_INSERTAR_USUARIO.replace("param1", usuario.getUserName()).replace("param2", usuario.getUserIdentificationNumber()).replace("param3", usuario.getUserNivelProfesional()).replace("param4", usuario.getUserCelular()).replace("param5", usuario.getUserEmail()).replace("param6", usuario.getUserCity()).replace("param7", usuario.getUserPassword()));
  
            String output = response.getEntity(String.class);
                
            if (output != null && !output.isEmpty()) {
                JsonObject responseUsuario = (JsonObject) new JsonParser().parse(output);
                String status = responseUsuario.get("code").getAsString();
                
                if (status != null && !status.isEmpty() && status.equals("106")) {
                    respuesta =  true;
                }
            }
            return respuesta;
        } catch (Exception e) {
            e.printStackTrace();
            _logger.log(Level.WARNING, "ERROR - ForoDelegate.consultaUsuarioPorCedula, al consultar servicio REST usuarios por dni {0}", e.getMessage());
            return respuesta;
        }
    }
}
