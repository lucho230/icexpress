<%-- 
    Document   : sesion
    Created on : 3/12/2016, 03:48:57 PM
    Author     : LUIS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.distrital.icexpress.platform.model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${responseUser != null && responseUser == 'success'}">
             <p>El usuario se ha insertado con exito! </p>
        </c:if>
        <c:if test="${responseUser != null && responseUser == 'error'}">
             <p>El usuario no se ha podido insertar </p>
        </c:if>
        <h1>Registrarse es muy sencillo!</h1>
        <form name="fmRegistro" method="POST" action="/user/registro">
             <label for="male">Nombre</label>
             <input type="text" name="user_name" id="user_name" />
             <label for="male">Cedula</label>
             <input type="text" name="user_cedula" id="user_cedula" />
             <label for="male">Contraseña</label>
             <input type="password" name="user_pass" id="user_pass" />
             <label for="male">Nivel Profesional</label>
             <input type="text" name="user_profesion" id="user_profesion" />
             <label for="male">Numero telefonico</label>
             <input type="text" name="user_telefono" id="user_telefono" />
             <label for="male">Correo Electronico</label>
             <input type="text" name="user_email" id="user_email" />
             <label for="male">Ciudad</label>
             <input type="text" name="user_ciudad" id="user_ciudad" />
             <input type="submit" value="Enviar"/>
        </form>
    </body>
</html>
