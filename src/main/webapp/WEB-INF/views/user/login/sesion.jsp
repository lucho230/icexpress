<%-- 
    Document   : sesion
    Created on : 3/12/2016, 03:48:57 PM
    Author     : LUIS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.distrital.icexpress.platform.model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenido a IC-EXPRESS</title>
    </head>
    <body>
        <h3>BIENVENIDO <c:out value="${usuario.userName}" /></h3>
        
        <div>
            <c:if test="${response == 'success'}">
                <p>SE HA GUARDADO EL TEMA CON EXITO! </p>
            </c:if>
            
        </div>
        <form name="fmSesion" action="/user/foro" method="POST">
            <input type="text" id="foro_nombre" name="foro_nombre" placeholder="Nombre" />
            <input type="text" id="foro_contenido" name="foro_contenido" placeholder="Contenido" />
            <input type="submit" value="Publicar"/>
        </form>
 
    </body>
</html>
